import React from 'react'
import styled from 'styled-components'
import ServiceImg from '../../assets/images/bg_1.jpg'
import { Container, Col, Row } from 'react-bootstrap'

const ProductsHome
    = () => {
        return (
            <Products>
                <Container className="py-5">
                    <h2 className="text-center">Menu:</h2>
                    <Row>
                        <Item>Bolos</Item>
                    </Row>
                </Container>
            </Products>
        )
    }

export default ProductsHome



const Products = styled.div`
      height: 400px;
    width: 100%;
    background-image: url(${ServiceImg})
`

const Item = styled(Col)`
    text-align: center;
    background: #ccc6;
    height: 200px;
    margin: 5px;
`