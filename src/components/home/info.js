
import React from 'react'
import { Container } from 'react-bootstrap'
import styled from 'styled-components'
import Bg_info from '../../assets/images/bg_info.jpg'
import Img_casa from '../../assets/images/casa.png'
import Img_welcome from '../../assets/images/bem_vindo.jpg'

const InfoHome = () => {
    return (
        <Info>
            <Container>
                <img src={Img_welcome} className="info_welcome" alt="welcome deus" />   
                <img src={Img_casa} className="img_info" alt="bem vindo" />
                <img src={Bg_info} className="bg_info" alt="background Info" />
               
            </Container>
        </Info>
    )
}

export default InfoHome


const Info = styled.div`
    height: 500px;
    width: 100%;
    background-size: 60% 100%;
    background-image: url(${Bg_info});
    background-size: 60% 100%;
    overflow: hidden;
  

    .img_info {
        width: 40%;
        display: flex;
        overflow: hidden;
        display: inline-block;
    }
`
