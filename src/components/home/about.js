
import React from 'react'
import styled from 'styled-components'
import { Row, Col } from 'react-bootstrap'
import AboutImg from '../../assets/images/about.jpg'
import Img_aviso from '../../assets/images/hermes.png'

const AboutHome = () => {
    return (
        <About>
            <Row>
                <Col>
                    <img src={AboutImg} alt="mesa de bolos" />
                </Col>
                <Col className="hermes">
                    <img src={Img_aviso} alt="avisos" />
                </Col>
                <Col>
                    <div className="description">
                        <h1>Avisos de Hermes:</h1>
                        <p>Hoje todos serão agraciados com o cupom de FRETE GRÀTIS concedido por Zeus.</p>
                    </div>
                </Col>
            </Row>

        </About>
    )
}

export default AboutHome


const About = styled.div`
    width: 100%;
    background: #fdcfca;
    color: #fff;
    min-height: 400px;

    .description{
        padding: 20px;
        display:flex;
        flex-direction:column;
        justify-content: center;
        color: #3e0211
    }
    img{
        max-height: 400px;
    }

    .hermes {
        display: flex;
        flex-direction: left;
    }
`
