import React from 'react'
import { Nav, Navbar, Container } from 'react-bootstrap'
import styled from 'styled-components'
import { GiCakeSlice } from 'react-icons/gi'
import { NavLink } from 'react-router-dom'

export default (props) => {

    const menu = [
        {
            title: "Principal",
            link: ''
        },
        {
            title: "História",
            link: 'sobre'
        },
        {
            title: "Bolos",
            link: 'produtos'
        },
        {
            title: "Serviços",
            link: 'servicos'
        },
        {
            title: "Fale Conosco",
            link: 'contato'
        }

    ]

    return (
        <Header>
            <Container>
                <Navbar expand="lg" variant="dark">
                    <Navbar.Brand href="#home">
                        <Logo>
                        <span><GiCakeSlice /> Bolo
                            <br />
                            dos Deuses</span>
                        </Logo>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse className="justify-content-end">
                        <Nav>
                            {menu.map((item, i) => (
                                <NavLink exact={true} to={item.link} key={i}>
                                    <Nav.Link as="div">{item.title}</Nav.Link>
                                </NavLink>
                            ))}
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </Container>
        </Header >
    )
}



const Header = styled.div`

    

    background-color: #00bfff;
    font-family: 'Lobster', sans-serif;
    a{
        text-decoration: none;
    }

    .nav-link{
        
        :hover{
            color: #95051c !important;
            font-weight: 500;
            text-decoration: none;
            font-size: 25px;
        }
    }
`

const Logo = styled.div`
    font-size: 20px;
    font-Weight: 600;
    margin:0;
    font-family: 'Lobster', sans-serif;

    svg{
        color: #F8E0E6;
        margin: -5px 1px;
    }

    span{
        color:  #F8E0E6;
        margin:0;
        font-size: 25px;
        text-transform: uppercase;
        display: block;
        text-align: center;
        letter-spacing: 4px;
    }
`

