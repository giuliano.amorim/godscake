import React from 'react'
import { Navbar, Nav, Form, FormControl, Button, Tabs, Card } from 'react-bootstrap'
import styled from 'styled-components'

export default () => {
    return (
        <Product>
            <Navbar bg="primary" variant="dark">
                <Navbar.Brand href="#home">Tipos:</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link href="#home">Todos</Nav.Link>
                    <Nav.Link href="#features">Bolos comuns</Nav.Link>
                    <Nav.Link href="#pricing">Bolos de casamento</Nav.Link>
                    <Nav.Link href="#pricing">Bolos de aniversário</Nav.Link>
                    <Nav.Link href="#pricing">Bolos de pote</Nav.Link>
                    <Nav.Link href="#pricing">Faça o seu bolo</Nav.Link>
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-light">Search</Button>
                </Form>
            </Navbar>
            <Card>
                <Card.Img variant="top" src="holder.js/100px160" />
                <Card.Body>
                    <Card.Title>Card title</Card.Title>
                    <Card.Text>
                        This is a wider card with supporting text below as a natural lead-in to
                        additional content. This content is a little bit longer.
      </Card.Text>
                </Card.Body>
                <Card.Footer>
                    <small className="text-muted">Last updated 3 mins ago</small>
                </Card.Footer>
            </Card>
            <Card>
                <Card.Img variant="top" src="holder.js/100px160" />
                <Card.Body>
                    <Card.Title>Card title</Card.Title>
                    <Card.Text>
                        This card has supporting text below as a natural lead-in to additional
        content.{' '}
                    </Card.Text>
                </Card.Body>
                <Card.Footer>
                    <small className="text-muted">Last updated 3 mins ago</small>
                </Card.Footer>
            </Card>
            <Card>
                <Card.Img variant="top" src="holder.js/100px160" />
                <Card.Body>
                    <Card.Title>Card title</Card.Title>
                    <Card.Text>
                        This is a wider card with supporting text below as a natural lead-in to
                        additional content. This card has even longer content than the first to
                        show that equal height action.
      </Card.Text>
                </Card.Body>
                <Card.Footer>
                    <small className="text-muted">Last updated 3 mins ago</small>
                </Card.Footer>
            </Card>
        </Product>
    )
}


const Product = styled.div`
    display:block;
    height: 500px;

    .tab-content{
        background: #eee !important;
    }
`

const TabBox = styled(Tabs)`
    background: #fff;

`
