import React from 'react'
import { Navbar, Card, CardGroup, Container } from 'react-bootstrap'
import styled from 'styled-components'



export default () => {
  return (
    <About>
      <Navbar bg="primary" variant="dark">
        <Navbar.Brand href="#home">Historia</Navbar.Brand>

      </Navbar>

      <Description>
        <Container>
          <h3>A empresa foi fundada em 1544 e bla bla bla....</h3>
          <h3>Fotos antigas .... TBT</h3>
        </Container>
      </Description>

      <CardGroup>
        <Card>
          <Card.Img variant="top" src="holder.js/100px160" />
          <Card.Body>
            <Card.Title>Card title</Card.Title>
            <Card.Text>
              This is a wider card with supporting text below as a natural lead-in to
              additional content. This content is a little bit longer.
      </Card.Text>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">Last updated 3 mins ago</small>
          </Card.Footer>
        </Card>
        <Card>
          <Card.Img variant="top" src="holder.js/100px160" />
          <Card.Body>
            <Card.Title>Card title</Card.Title>
            <Card.Text>
              This card has supporting text below as a natural lead-in to additional
        content.{' '}
            </Card.Text>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">Last updated 3 mins ago</small>
          </Card.Footer>
        </Card>
        <Card>
          <Card.Img variant="top" src="holder.js/100px160" />
          <Card.Body>
            <Card.Title>Card title</Card.Title>
            <Card.Text>
              This is a wider card with supporting text below as a natural lead-in to
              additional content. This card has even longer content than the first to
              show that equal height action.
      </Card.Text>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">Last updated 3 mins ago</small>
          </Card.Footer>
        </Card>
      </CardGroup>
    </About>
  )
}


const About = styled.div`
    display:block;
`
const Description = styled.div`
    height: 200px;
    background: #fff4;
    width: 100%;
    display:flex;
`


