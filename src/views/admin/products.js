import React, { useEffect, useState } from 'react'
import ListProducts from '../../components/product/list'
import FormProducts from '../../components/product/form'
import { Button } from 'react-bootstrap'
import styled from 'styled-components'
import { useLocation } from 'react-router-dom'

export default () => {
    const [isForm, setForm] = useState(false)
    const [update, setUpdate] = useState({})
    const location = useLocation();

    const updateProduct = (prd) => {
        setUpdate(prd)
        setForm(true)
    }

    useEffect(() => {
        if (location.state?.update) {
            setForm(false)
        }
    }, [location])

    return (
        <Products>
            <Button size="sm" variant="success" onClick={() => setForm(!isForm)}>
                {isForm ? "Lista" : "Novo"}
            </Button>
            <hr />
            { isForm
                ? <FormProducts update={update} />
                : <ListProducts updateProduct={updateProduct} />
            }
        </Products>
    )
}

const Products = styled.div`
 
`
